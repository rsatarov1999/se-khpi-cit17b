package task04;

import task03.View;
import task03.ViewableResult;

/**
 * ConcreteCreator
 * (шаблон проектирования
 * Factory Method)<br>
 * Объявляет метод,
 * "фабрикующий" объекты
 *
 * @author Ruslan Satarov
 * @version 1.0
 * @see ViewableResult
 * @see ViewableTable#getView()
 */

public class ViewableTable extends ViewableResult {
    /**
     * Создаёт отображаемый объект {@linkplain ViewTable}
     */
    @Override
    public View getView() {
        return new ViewTable();
    }
}
