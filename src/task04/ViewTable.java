package task04;

import task03.ViewResult;

/** ConcreteProduct
 * (шаблон проектирования
 * Factory Method)<br>
 * Вывод в виде таблицы
 * @author Ruslan Satarov
 * @version 1.0
 * @see ViewResult
 */
public class ViewTable extends ViewResult {
    /** Ширина таблицы */
    private int width;

    /**
     * Вызывается конструктор суперкласса {@linkplain ViewResult#ViewResult() ViewResult()}
     */
    public ViewTable() {
        width = 43;
    }

    /** Выводит вертикальный разделитель шириной {@linkplain ViewTable#width} символов */
    private void outLine() {
        for(int i = 0; i < width; i++) {
            System.out.print('-');
        }
    }

    /** Вызывает {@linkplain ViewTable#outLine()}; завершает вывод разделителем строки */
    private void outLineLn() {
        outLine();
        System.out.println();
    }

    /** Выводит заголовок таблицы шириной {@linkplain ViewTable#width} символов */
    private void outHeader() {
        System.out.println("     Number     | Hex Length | Octal Length");
    }

    /** Выводит тело таблицы шириной {@linkplain ViewTable#width} символов */
    private void outBody() {
        System.out.printf("%15d | %10d | %12d\n", result.getNumber(), result.getHexLength(), result.getOctalLength());
    }

    /** Вывод элемента таблицы<br>{@inheritDoc} */
    @Override
    public void header() {
        outHeader();
        outLineLn();
    }

    /** Вывод элемента таблицы<br>{@inheritDoc} */
    @Override
    public void body() {
        outBody();
    }

    /** Вывод элемента таблицы<br>{@inheritDoc} */
    @Override
    public void footer() {
        outLineLn();
    }
}
