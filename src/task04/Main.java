package task04;

import task03.View;

/**
 * Вычисление и отображение результатов.
 * Содержит реализацию статического метода main().
 *
 * @author Ruslan Satarov
 * @version 1.0
 * @see Main#main
 */
public class Main extends task03.Main {

    public Main(View view) {
        super(view);
    }

    /**
     * Выполняется при запуске программы.
     * Вычисляется значение функции для различных аргументов.
     * Результаты вычислений выводятся на экран.
     *
     * @param args - параметры запуска программы.
     */
    public static void main(String[] args) {
        Main main = new Main(new ViewableTable().getView());
        main.menu();
    }
}
