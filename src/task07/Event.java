package task07;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Аннотация времени
 * выполнения для
 * назначения
 * методам наблюдателя
 * конкретных событий
 *
 * @author Ruslan Satarov
 * @see AnnotatedObserver
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Event {
    String value();
}
