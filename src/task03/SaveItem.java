package task03;

import java.io.Serializable;

/**
 * Хранит исходные данные и результат вычислений.
 *
 * @author Ruslan Satarov
 * @version 1.0
 */
public class SaveItem implements Serializable {

    /**
     * Исходное число.
     */
    private int number;

    /**
     * Длина числа в 16-ричном представлении.
     */
    private int hexLength;

    /**
     * Длина числа в 8-ричном представлении.
     */
    private int octalLength;

    /**
     * Пустой конструктор.
     */
    public SaveItem() {
    }

    /**
     * Устанавливает значения полей: исходного числа
     * и длин числа в 16-ричном и 8-ричном представлениях.
     *
     * @param number      - значение для инициализации поля {@linkplain SaveItem#number}
     * @param hexLength   - значение для инициализации поля {@linkplain SaveItem#hexLength}
     * @param octalLength - значение для инициализации поля {@linkplain SaveItem#octalLength}
     */
    public SaveItem(int number, int hexLength, int octalLength) {
        this.number = number;
        this.hexLength = hexLength;
        this.octalLength = octalLength;
    }

    /**
     * Получение значения поля {@linkplain SaveItem#number}
     *
     * @return Значение {@linkplain SaveItem#number}
     */
    public int getNumber() {
        return number;
    }

    /**
     * Установка значения поля {@linkplain SaveItem#number}
     *
     * @param number - значение для {@linkplain SaveItem#number}
     */
    public void setNumber(int number) {
        this.number = number;
    }

    /**
     * Получение значения поля {@linkplain SaveItem#hexLength}
     *
     * @return Значение {@linkplain SaveItem#hexLength}
     */
    public int getHexLength() {
        return hexLength;
    }

    /**
     * Установка значения поля {@linkplain SaveItem#hexLength}
     *
     * @param hexLength - значение для {@linkplain SaveItem#hexLength}
     */
    public void setHexLength(int hexLength) {
        this.hexLength = hexLength;
    }

    /**
     * Получение значения поля {@linkplain SaveItem#octalLength}
     *
     * @return Значение {@linkplain SaveItem#octalLength}
     */
    public int getOctalLength() {
        return octalLength;
    }

    /**
     * Установка значения поля {@linkplain SaveItem#octalLength}
     *
     * @param octalLength - значение для {@linkplain SaveItem#octalLength}
     */
    public void setOctalLength(int octalLength) {
        this.octalLength = octalLength;
    }

    /**
     * Установка значений {@linkplain SaveItem#number},
     * {@linkplain SaveItem#hexLength} и {@linkplain SaveItem#octalLength}
     *
     * @param number      - значение для {@linkplain SaveItem#number}
     * @param hexLength   - значение для {@linkplain SaveItem#hexLength}
     * @param octalLength - значение для {@linkplain SaveItem#octalLength}
     */
    public void setAll(int number, int hexLength, int octalLength) {
        this.number = number;
        this.hexLength = hexLength;
        this.octalLength = octalLength;
    }

    /**
     * Представляет результат вычислений в виде строки.<br>{@inheritDoc}
     */
    @Override
    public String toString() {
        return "SaveItem{" +
                "number=" + number +
                ", hexLength=" + hexLength +
                ", octalLength=" + octalLength +
                '}';
    }

    /**
     * Автоматически сгенерированный метод.<br>{@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SaveItem saveItem = (SaveItem) o;
        return number == saveItem.number
                && hexLength == saveItem.hexLength
                && octalLength == saveItem.octalLength;
    }
}
