package task03;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Содержит реализацию методов для вычисления и отображения результатов.
 *
 * @author Ruslan Satarov
 * @version 1.0
 */
public class ViewResult implements View {

    /**
     * Имя файла, используемое при сериализации.
     */
    private static final String FILE_NAME = "SaveItem.sav";

    /**
     * Сохраняет результат вычислений. Объект класса {@linkplain SaveItem}
     */
    protected SaveItem result;

    /**
     * Инициализирует {@linkplain ViewResult#result}
     */
    public ViewResult() {
        result = new SaveItem();
    }

    /**
     * Получить значение {@linkplain ViewResult#result}
     *
     * @return текущее значение ссылки на объект {@linkplain SaveItem}
     */
    public SaveItem getResult() {
        return result;
    }

    /**
     * Установить значение {@linkplain ViewResult#result}
     *
     * @param result - новое значение ссылки на объект {@linkplain SaveItem}
     */
    public void setResult(SaveItem result) {
        this.result = result;
    }

    /**
     * Вычисляет количество цифр в 16-ричном представлении числа.
     *
     * @param number - исходное число.
     * @return количество цифр в 16-ричном представлении числа.
     */
    private int calcHexLength(int number) {
        return Integer.toHexString(Math.abs(number)).length();
    }

    /**
     * Вычисляет количество цифр в 8-ричном представлении числа.
     *
     * @param number - исходное число.
     * @return количество цифр в 8-ричном представлении числа.
     */
    private int calcOctalLength(int number) {
        return Integer.toOctalString(Math.abs(number)).length();
    }

    public void init() {
        init((int) (Math.random() * 10000000));
    }

    /**
     * Вычисляет количество цифр в 8-ричном и 16-ричном представлениях числа
     * и сохраняет результат в объекте {@linkplain ViewResult#result}
     *
     * @param number - исходное число.
     */
    public void init(int number) {
        result.setAll(number,
                calcHexLength(number),
                calcOctalLength(number));
    }

    @Override
    public void header() {
        System.out.println("Result:");
    }

    @Override
    public void body() {
        System.out.println(result);
    }

    @Override
    public void footer() {
        System.out.println("End.");
    }

    /**
     * Выводит результат вычислений.
     */
    public void show() {
        header();
        body();
        footer();
    }

    /**
     * Сохраняет {@linkplain ViewResult#result} в файле {@linkplain ViewResult#FILE_NAME}
     *
     * @throws IOException в случае возникновения проблем при сохранении.
     */
    public void save() throws IOException {
        try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FILE_NAME))) {
            os.writeObject(result);
        }
    }

    /**
     * Восстанавливает {@linkplain ViewResult#result} из файла {@linkplain ViewResult#FILE_NAME}
     *
     * @throws Exception в случае возникновения проблем при восстановлении.
     */
    public void restore() throws Exception {
        try (ObjectInputStream is = new ObjectInputStream(new FileInputStream(FILE_NAME))) {
            result = (SaveItem) is.readObject();
        }
    }
}
