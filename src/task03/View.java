package task03;

/**
 * Product (шаблон проектирования Factory Method)<br>
 * Интерфейс "фабрикуемых" объектов<br>
 * Объявляет методы отображения объектов
 *
 * @author Ruslan Satarov
 * @version 1.0
 */
public interface View {

    /**
     * Выполняет инициализацию.
     */
    void init();

    /**
     * Отображает заголовок.
     */
    void header();

    /**
     * Отображает основную часть.
     */
    void body();

    /**
     * Отображает окончание.
     */
    void footer();

    /**
     * Отображает объект целиком.
     */
    void show();

    /**
     * Сохраняет данные для последующего восстановления.
     */
    void save() throws Exception;

    /**
     * Восстанавливает ранее сохранённые данные.
     */
    void restore() throws Exception;
}
