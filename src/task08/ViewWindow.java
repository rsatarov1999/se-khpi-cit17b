package task08;

import task06.ViewResult;

import java.awt.*;

/**
 * ConcreteProduct
 * (шаблон проектирования
 * Factory Method);
 * отображение графика
 *
 * @author Ruslan Satarov
 * @version 1.0
 * @see ViewResult
 * @see Window
 */
public class ViewWindow extends ViewResult {

    /**
     * Отображаемое окно
     */
    private Window window = null;

    /**
     * Создание и отображение окна
     */
    public ViewWindow() {
        super();
        window = new Window(this);
        window.setSize(new Dimension(640, 200));
        window.setResizable(false);
        window.setTitle("Result");
        window.setVisible(true);
    }

    @Override
    public void show() {
        super.show();
        window.setVisible(true);
        window.repaint();
    }

}