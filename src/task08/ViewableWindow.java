package task08;

import task03.View;
import task03.Viewable;

/**
 * ConcreteCreator
 * (шаблон проектирования
 * Factory Method);
 * реализует метод,
 * "фабрикующий" объекты
 *
 * @author Ruslan Satarov
 * @version 1.0
 * @see Viewable
 * @see Viewable#getView()
 */
public class ViewableWindow implements Viewable {
    @Override
    public View getView() {
        return new ViewWindow();
    }
}
