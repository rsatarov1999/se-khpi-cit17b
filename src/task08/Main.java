package task08;

import task05.Application;

/** Вычисление и отображение
 * результатов; cодержит реализацию
 * статического метода main()
 * @author Ruslan Satarov
 * @version 7.0
 * @see Main#main
 */
public class Main {

    /** Выполняется при запуске программы
     * @param args параметры запуска программы
     */
    public static void main(String[] args) {
        Application app = Application.getInstance();
        app.run(new ViewableWindow());
        System.exit(0);
    }

}