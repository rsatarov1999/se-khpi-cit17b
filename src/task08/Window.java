package task08;

import task03.SaveItem;
import task06.ViewResult;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;

/**
 * Создание окна
 * отображения графика
 *
 * @author Ruslan Satarov
 * @version 1.0
 * @see Frame
 */
@SuppressWarnings("serial")
public class Window extends Frame {

    /**
     * Толщина отступа от края окна
     */
    private static final int BORDER = 20;

    /**
     * Объект, реализующий интерфейс {@linkplain task03.View};<br>
     * обслуживает коллекцию объектов {@linkplain task03.SaveItem}
     */
    private ViewResult view;

    /**
     * Инициализирует {@linkplain Window#view};<br>
     * Создает обработчик события закрытия окна
     *
     * @param view значение для поля {@linkplain Window#view}
     */
    public Window(ViewResult view) {
        this.view = view;
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                //System.exit(0);
                setVisible(false);
            }
        });
    }

    @Override
    public void paint(Graphics g) {
        Rectangle r = getBounds();
        Rectangle c = new Rectangle();
        r.x = BORDER;
        r.y = 25 + BORDER;
        r.width -= r.x + BORDER;
        r.height -= r.y + BORDER;
        c.x = r.x;
        c.y = r.y;
        c.width = r.width;
        c.height = r.height / 2;
        g.setColor(Color.LIGHT_GRAY);
        g.setColor(Color.RED);
        g.drawLine(c.x, c.y, c.x + c.width, c.y);
        g.drawLine(c.x, r.y, c.x, r.y + r.height);
        double maxX = 10000000;
        g.drawString("+" + maxX, c.x + c.width -
                g.getFontMetrics().stringWidth("+" + maxX), c.y);
        g.setColor(Color.BLUE);
        for (SaveItem item : view.getItems()) {
            double position = 600.0 * item.getNumber() / 10000000.0;
            g.drawOval(c.x + (int) (position), c.y, 10, 10);
        }
        Graphics2D g2 = (Graphics2D) g;
        AffineTransform transform = new AffineTransform();

        for (SaveItem item : view.getItems()) {
            double position = 600.0 * item.getNumber() / 10000000.0;
            transform.setToRotation(Math.toRadians(90), c.x + position, c.y);
            g2.setTransform(transform);
            g2.drawString("" + item.getNumber(), (int) (c.x + position + 15), c.y);
        }
    }

}