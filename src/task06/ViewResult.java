package task06;

import task03.SaveItem;
import task03.View;

import java.io.*;
import java.util.ArrayList;

/**
 * Содержит реализацию методов для вычисления и отображения результатов.
 *
 * @author Ruslan Satarov
 * @version 1.0
 */
public class ViewResult implements View {

    /**
     * Имя файла, используемое при сериализации.
     */
    private static final String FILE_NAME = "SaveItem.sav";

    /**
     * Сохраняет результат вычислений. Объект класса {@linkplain SaveItem}
     */
    protected ArrayList<SaveItem> result = new ArrayList<>();

    /**
     * Инициализирует {@linkplain ViewResult#result}
     */
    public ViewResult() {
        for (int i = 0; i < 10; i++) {
            result.add(new SaveItem());
        }
    }

    /**
     * Получить значение {@linkplain ViewResult#result}
     *
     * @return текущее значение ссылки на объект {@linkplain ArrayList}
     */
    public ArrayList<SaveItem> getItems() {
        return result;
    }

    /**
     * Вычисляет количество цифр в 16-ричном представлении числа.
     *
     * @param number - исходное число.
     * @return количество цифр в 16-ричном представлении числа.
     */
    private int calcHexLength(int number) {
        return Integer.toHexString(Math.abs(number)).length();
    }

    /**
     * Вычисляет количество цифр в 8-ричном представлении числа.
     *
     * @param number - исходное число.
     * @return количество цифр в 8-ричном представлении числа.
     */
    private int calcOctalLength(int number) {
        return Integer.toOctalString(Math.abs(number)).length();
    }

    public void init() {
        for (SaveItem saveItem : result) {
            int number = (int) (Math.random() * 10000000);
            saveItem.setAll(number,
                    calcHexLength(number),
                    calcOctalLength(number));
        }
    }

    @Override
    public void header() {
        System.out.println("Result:");
    }

    @Override
    public void body() {
        for (SaveItem saveItem : result) {
            System.out.println(saveItem);
        }
    }

    @Override
    public void footer() {
        System.out.println("End.");
    }

    /**
     * Выводит результат вычислений.
     */
    public void show() {
        header();
        body();
        footer();
    }

    /**
     * Сохраняет {@linkplain ViewResult#result} в файле {@linkplain ViewResult#FILE_NAME}
     *
     * @throws IOException в случае возникновения проблем при сохранении.
     */
    public void save() throws IOException {
        try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FILE_NAME))) {
            os.writeObject(result);
        }
    }

    /**
     * Восстанавливает {@linkplain ViewResult#result} из файла {@linkplain ViewResult#FILE_NAME}
     *
     * @throws Exception в случае возникновения проблем при восстановлении.
     */
    @SuppressWarnings("unchecked")
    public void restore() throws Exception {
        try (ObjectInputStream is = new ObjectInputStream(new FileInputStream(FILE_NAME))) {
            result = (ArrayList<SaveItem>) is.readObject();
        }
    }
}
