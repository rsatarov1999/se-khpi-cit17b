package task06;


import task03.View;
import task05.GenerateConsoleCommand;
import task05.Menu;
import task05.ViewConsoleCommand;

/**
 * Вычисление и отображение
 * результатов; содержит реализацию
 * статического метода main()
 *
 * @author Ruslan Satarov
 * @version 5.0
 * @see Main#main
 */
public class Main {

    /**
     * Объект, реализующий интерфейс {@linkplain View};
     * обслуживает коллекцию объектов;
     * инициализируется с помощью Factory Method
     */
    private View view = new ViewableResult().getView();

    /**
     * Объект класса {@linkplain Menu};
     * макрокоманда (шаблон Command)
     */
    private Menu menu = new Menu();

    /**
     * Обработка команд пользователя
     */
    public void run() {
        menu.add(new ViewConsoleCommand(view));
        menu.add(new GenerateConsoleCommand(view));
        menu.add(new ExecuteConsoleCommand(view));
        menu.execute();
    }

    /**
     * Выполняется при запуске программы
     *
     * @param args параметры запуска программы
     */
    public static void main(String[] args) {
        Main main = new Main();
        main.run();
    }

}
