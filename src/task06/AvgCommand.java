package task06;

import task03.SaveItem;
import task05.Command;

import java.util.concurrent.TimeUnit;

/**
 * Задача, используемая
 * обработчиком потока;
 * шаблон Worker Thread
 *
 * @author Ruslan Satarov
 * @version 1.0
 */
public class AvgCommand implements Command /*, Runnable */ {

    /**
     * Хранит результат обработки коллекции
     */
    private double result = 0.0;

    /**
     * Флаг готовности результата
     */
    private int progress = 0;

    /**
     * Обслуживает коллекцию объектов
     */
    private ViewResult viewResult;

    /**
     * Возвращает поле {@linkplain AvgCommand#viewResult}
     *
     * @return значение {@linkplain AvgCommand#viewResult}
     */
    public ViewResult getViewResult() {
        return viewResult;
    }

    /**
     * Устанавливает поле {@linkplain AvgCommand#viewResult}
     *
     * @param viewResult значение для {@linkplain AvgCommand#viewResult}
     * @return новое значение {@linkplain AvgCommand#viewResult}
     */
    public ViewResult setViewResult(ViewResult viewResult) {
        return this.viewResult = viewResult;
    }

    /**
     * Инициализирует поле {@linkplain AvgCommand#viewResult}
     *
     * @param viewResult объект класса {@linkplain ViewResult}
     */
    public AvgCommand(ViewResult viewResult) {
        this.viewResult = viewResult;
    }

    /**
     * Возвращает результат
     *
     * @return поле {@linkplain AvgCommand#result}
     */
    public double getResult() {
        return result;
    }

    /**
     * Проверяет готовность результата
     *
     * @return false - если результат найден, иначе - true
     * @see AvgCommand#result
     */
    public boolean running() {
        return progress < 100;
    }

    /**
     * Используется обработчиком потока {@linkplain CommandQueue};
     * шаблон Worker Thread
     */
    @Override
    public void execute() {
        progress = 0;
        System.out.println("Average executed...");
        result = 0.0;
        int idx = 1, size = viewResult.getItems().size();
        for (SaveItem item : viewResult.getItems()) {
            result += item.getNumber();
            progress = idx * 100 / size;
            if (idx++ % (size / 2) == 0) {
                System.out.println("Average " + progress + "%");
            }
            try {
                TimeUnit.MILLISECONDS.sleep(2000 / size);
            } catch (InterruptedException e) {
                System.err.println(e);
            }
        }
        result /= size;
        System.out.println("Average done. Result = " + String.format("%.2f", result));
        progress = 100;
    }

    /*
     @Override
     public void run() {
     execute();
     }
     */

}