package task05;

/**
 * Интерфейс
 * консольной команды;
 * шаблон Command
 *
 * @author Ruslan Satarov
 * @version 1.0
 */
public interface ConsoleCommand extends Command {
    /**
     * Горячая клавиша команды;
     * шаблон Command
     *
     * @return символ горячей клавиши
     */
    char getKey();
}
