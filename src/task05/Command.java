package task05;

/**
 * Интерфейс команды
 * или задачи;
 * шаблоны: Command,
 * Worker Thread
 *
 * @author Ruslan Satarov
 * @version 1.0
 */
public interface Command {
    /**
     * Выполнение команды; шаблоны: Command, Worker Thread
     */
    void execute();
}
