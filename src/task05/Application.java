package task05;

import task03.View;
import task03.Viewable;
import task04.ViewableTable;

/**
 * Формирует и отображает
 * меню; реализует шаблон
 * Singleton
 *
 * @author Ruslan Satarov
 * @version 1.0
 */
public class Application {

    /**
     * Объект класса {@linkplain Menu};
     * макрокоманда (шаблон Command)
     */
    private Menu menu;

    /**
     * Ссылка на экземпляр класса Application; шаблон Singleton
     *
     * @see Application
     */
    private static Application instance;

    /**
     * Закрытый конструктор; шаблон Singleton
     *
     * @see Application
     */
    private Application() {
    }

    /**
     * Возвращает ссылку на экземпляр класса Application;
     * шаблон Singleton
     *
     * @see Application
     */
    public static Application getInstance() {
        if (instance == null) {
            instance = new Application();
        }
        return instance;
    }

    /**
     * Обработка команд пользователя
     *
     * @see Application
     */
    public void run() {
        run(null);
    }

    /**
     * Обработка команд пользователя
     *
     * @see Application
     */
    public void run(Viewable viewable) {
        View view;
        if (viewable == null) {
            view = new ViewableTable().getView();
        } else {
            view = viewable.getView();
        }
        menu = new Menu();
        menu.add(new GenerateConsoleCommand(view));
        menu.add(new SaveConsoleCommand(view));
        menu.add(new RestoreConsoleCommand(view));
        menu.add(new ViewConsoleCommand(view));
        menu.execute();
    }
}
