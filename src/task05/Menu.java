package task05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Макрокоманда
 * (шаблон Command);
 * Коллекция объектов
 * класса ConsoleCommand
 *
 * @see ConsoleCommand
 */
public class Menu implements Command {

    /**
     * Коллекция консольных команд;
     *
     * @see ConsoleCommand
     */
    private List<ConsoleCommand> menu = new ArrayList<>();

    /**
     * Добавляет новую команду в коллекцию
     *
     * @param command реализует {@linkplain ConsoleCommand}
     * @return command
     */
    public ConsoleCommand add(ConsoleCommand command) {
        menu.add(command);
        return command;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Enter command...\n");
        for (ConsoleCommand command : menu) {
            sb.append(command).append(", ");
        }
        sb.append("'q'uit");
        return sb.toString();
    }

    @Override
    public void execute() {
        String input = null;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        menu:
        while (true) {
            do {
                System.out.println(this);
                try {
                    input = reader.readLine();
                } catch (IOException e) {
                    System.err.println("Error: " + e);
                    System.exit(0);
                }
            } while (input.length() < 1);
            char key = input.charAt(0);
            if (key == 'q') {
                System.out.println("Exit.");
                break;
            }
            for (ConsoleCommand command : menu) {
                if (input.charAt(0) == command.getKey()) {
                    command.execute();
                    continue menu;
                }
            }
            System.out.println("Wrong command.");
        }
    }

    /**
     * Возвращает список команд
     *
     * @return Список команд
     */
    public List<ConsoleCommand> getCommandList() {
        return menu;
    }
}
