package task05;

import task03.View;

/**
 * Консольная команда
 * Save;
 * шаблон Command
 *
 * @author Ruslan Satarov
 * @version 1.0
 */
public class SaveConsoleCommand implements ConsoleCommand {
    /**
     * Объект, реализующий интерфейс {@linkplain View};
     * обслуживает коллекцию объектов
     */
    private View view;

    /**
     * Инициализирует поле {@linkplain SaveConsoleCommand#view}
     *
     * @param view объект, реализующий интерфейс {@linkplain View}
     */
    public SaveConsoleCommand(View view) {
        this.view = view;
    }

    @Override
    public char getKey() {
        return 's';
    }

    @Override
    public void execute() {
        System.out.println("Save current.");
        try {
            view.save();
        } catch (Exception e) {
            System.err.println("Serialization error: " + e);
        }
        view.show();
    }

    @Override
    public String toString() {
        return "'s'ave";
    }
}
