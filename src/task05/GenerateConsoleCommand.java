package task05;

import task03.View;

/**
 * Консольная команда
 * Generate;
 * шаблон Command
 *
 * @author Ruslan Satarov
 * @version 1.0
 */
public class GenerateConsoleCommand implements ConsoleCommand {

    /**
     * Объект, реализующий интерфейс {@linkplain View};
     * обслуживает коллекцию объектов
     */
    private View view;

    /**
     * Инициализирует поле {@linkplain GenerateConsoleCommand#view}
     *
     * @param view объект, реализующий интерфейс {@linkplain View}
     */
    public GenerateConsoleCommand(View view) {
        this.view = view;
    }

    @Override
    public char getKey() {
        return 'g';
    }

    @Override
    public void execute() {
        System.out.println("Random generation.");
        view.init();
        view.show();
    }

    @Override
    public String toString() {
        return "'g'enerate";
    }
}
