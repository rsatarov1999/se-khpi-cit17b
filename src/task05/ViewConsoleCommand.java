package task05;

import task03.View;

/**
 * Консольная команда
 * View;
 * шаблон Command
 *
 * @author Ruslan Satarov
 * @version 1.0
 */
public class ViewConsoleCommand implements ConsoleCommand {
    /**
     * Объект, реализующий интерфейс {@linkplain View};
     * обслуживает коллекцию объектов
     */
    private View view;

    /**
     * Инициализирует поле {@linkplain ViewConsoleCommand#view}
     *
     * @param view объект, реализующий интерфейс {@linkplain View}
     */
    public ViewConsoleCommand(View view) {
        this.view = view;
    }

    @Override
    public char getKey() {
        return 'v';
    }

    @Override
    public void execute() {
        System.out.println("View current.");
        view.show();
    }

    @Override
    public String toString() {
        return "'v'iew";
    }
}
