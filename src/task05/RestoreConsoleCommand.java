package task05;

import task03.View;

/**
 * Консольная команда
 * Restore;
 * шаблон Command
 *
 * @author Ruslan Satarov
 * @version 1.0
 */
public class RestoreConsoleCommand implements ConsoleCommand {
    /**
     * Объект, реализующий интерфейс {@linkplain View};
     * обслуживает коллекцию объектов
     */
    private View view;

    /**
     * Инициализирует поле {@linkplain RestoreConsoleCommand#view}
     *
     * @param view объект, реализующий интерфейс {@linkplain View}
     */
    public RestoreConsoleCommand(View view) {
        this.view = view;
    }

    @Override
    public char getKey() {
        return 'r';
    }

    @Override
    public void execute() {
        System.out.println("Restore last saved.");
        try {
            view.restore();
        } catch (Exception e) {
            System.err.println("Serialization error: " + e);
        }
        view.show();
    }

    @Override
    public String toString() {
        return "'r'estore";
    }
}
