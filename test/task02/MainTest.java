package task02;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Выполняет тестирование разработанных классов.
 *
 * @author Ruslan Satarov
 * @version 1.0
 */
public class MainTest {

    /**
     * Проверка основной функциональности класса {@linkplain Calc}
     */
    @Test
    public void calcTest() {
        Calc calc = new Calc();
        int[] initNums = {1, 12, 1357, 12345678, 99999999};
        int[][] expected = {
                {1, 2, 4, 8, 9},
                {1, 1, 3, 6, 7}
        };

        for (int i = 0; i < 5; i++) {
            calc.init(initNums[i]);
            assertEquals(expected[0][i], calc.getResult().getOctalLength());
            assertEquals(expected[1][i], calc.getResult().getHexLength());
        }
    }

    /**
     * Проверка сериализации. Корректность восстановления данных.
     */
    @Test
    public void testRestore() throws Exception {
        Calc calc = new Calc();
        for (int i = 0; i < 1000; i++) {
            int number = (int) (Math.random() * 100000000);
            calc.init(number);
            int hexLength = calc.getResult().getHexLength();
            int octalLength = calc.getResult().getOctalLength();
            calc.save();
            calc.init((int) (Math.random() * 100000000));
            calc.restore();
            assertEquals(number, calc.getResult().getNumber());
            assertEquals(hexLength, calc.getResult().getHexLength());
            assertEquals(octalLength, calc.getResult().getOctalLength());
        }
    }
}
