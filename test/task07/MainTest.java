package task07;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Тестирование
 * разработанных классов
 *
 * @author Ruslan Satarov
 * @version 6.0
 * @see ItemsGenerator
 * @see ItemsSorter
 * @see Items
 */
public class MainTest {

    /**
     * Количество элементов в коллекции
     */
    private static final int ITEMS_SIZE = 1000;
    /**
     * Наблюдатель; шаблон Observer
     */
    private static ItemsGenerator generator = new ItemsGenerator();
    /**
     * Наблюдатель; шаблон Observer
     */
    private static ItemsSorter sorter = new ItemsSorter();
    /**
     * Наблюдаемый объект; шаблон Observer
     */
    private static Items observable = new Items();

    /**
     * Выполняется первым
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        observable.addObserver(generator);
        observable.addObserver(sorter);
    }

    /**
     * Тестирует операцию добавления объектов в коллекцию
     */
    @Test
    public void testAdd() {
        observable.getItems().clear();
        observable.add(new Item("AAA"));
        observable.add("AAA");
        observable.add("");
        observable.add(ITEMS_SIZE);
        for (Item item : observable) {
            assertFalse(item.getData().isEmpty());
        }
        assertEquals(ITEMS_SIZE + 3, observable.getItems().size());
    }

    /**
     * Тестирует операции добавления и удаления объектов
     */
    @Test
    public void testAddDel() {
        Item tmp;
        observable.getItems().clear();
        observable.add("");
        observable.add(ITEMS_SIZE);
        for (int i = ITEMS_SIZE; i > 0; i--) {
            tmp = observable.getItems().get((new Random()).nextInt(i));
            observable.del(tmp);
        }
        assertEquals(1, observable.getItems().size());
    }

    /**
     * Тестирует операцию сортировки объектов
     */
    @Test
    public void testSort() {
        observable.getItems().clear();
        observable.add(ITEMS_SIZE);
        List<Item> items = new ArrayList<Item>(observable.getItems());
        Collections.sort(items);
        assertEquals(items, observable.getItems());
    }

}