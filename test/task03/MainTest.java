package task03;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Выполняет тестирование разработанных классов.
 *
 * @author Ruslan Satarov
 * @version 1.0
 */
public class MainTest {

    /**
     * Проверка основной функциональности класса {@linkplain ViewResult}
     */
    @Test
    public void calcTest() {
        ViewResult viewResult = new ViewResult();
        int[] initNums = {1, 12, 1357, 12345678, 99999999};
        int[][] expected = {
                {1, 2, 4, 8, 9},
                {1, 1, 3, 6, 7}
        };

        for (int i = 0; i < 5; i++) {
            viewResult.init(initNums[i]);
            assertEquals(expected[0][i], viewResult.getResult().getOctalLength());
            assertEquals(expected[1][i], viewResult.getResult().getHexLength());
        }
    }

    /**
     * Проверка сериализации. Корректность восстановления данных.
     */
    @Test
    public void testRestore() throws Exception {
        ViewResult viewResult = new ViewResult();
        for (int i = 0; i < 1000; i++) {
            int number = (int) (Math.random() * 100000000);
            viewResult.init(number);
            int hexLength = viewResult.getResult().getHexLength();
            int octalLength = viewResult.getResult().getOctalLength();
            viewResult.save();
            viewResult.init((int) (Math.random() * 100000000));
            viewResult.restore();
            assertEquals(number, viewResult.getResult().getNumber());
            assertEquals(hexLength, viewResult.getResult().getHexLength());
            assertEquals(octalLength, viewResult.getResult().getOctalLength());
        }
    }
}
