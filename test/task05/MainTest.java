package task05;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Тестирование класса Menu
 *
 * @author Ruslan Satarov
 * @version 4.0
 */
public class MainTest {

    /** Проверка метода {@linkplain Menu#add} */
    @Test
    public void testAddConsoleCommand() {
        Menu menu = new Menu();
        menu.add(new GenerateConsoleCommand(null));
        menu.add(new RestoreConsoleCommand(null));
        menu.add(new SaveConsoleCommand(null));
        menu.add(new ViewConsoleCommand(null));

        assertEquals(4, menu.getCommandList().size());
    }
}
